function *fibonacciFn()
{
    var first=0;
    yield first;

    var second=1;
    yield second;
    var sum=0;
    while(true)
    {
        sum=first+second;
        yield sum;
        first=second;
        second=sum;
    }
}
var obj=fibonacciFn();
module.exports=
{
    fibonacciFn:fibonacciFn
}
