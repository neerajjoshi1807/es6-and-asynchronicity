var fs=require("fs");
function read(file)
{
    return new Promise((resolve,reject)=>
    {
        fs.readFile(file,"utf8",(err,data)=>
        {
            if(!err)
            resolve(data);
            else
            reject(err);
        });
    });
};
async function display(pathToFile)
{
    var c=await read(pathToFile);
    return(c);
}
module.exports = {
  content: display
};


