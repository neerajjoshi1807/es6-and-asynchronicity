function timeout(num)
{
    return new Promise((resolve,reject)=>
    {   
        setTimeout(()=>
        {
            resolve(num+" ms timer finished");
        },num);

    });
}
function resolveAfter2Second()
{
    console.log( "starting slow promise");
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            resolve(2000);
        },2000);

    });
};
function resolveAfter1Second()
{
    console.log( "starting fast promise");
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            resolve(1000);
        },1000);

    });
};
async function concurrentStart()
{
    console.log("==CONCURRENT START with await==");
    var pro1=await resolveAfter2Second();
    var pro2=await resolveAfter1Second();
    var promises=[];
    promises.push(pro1);
    promises.push(pro2);
    return (await Promise.all(promises));

};
module.exports = concurrentStart;
