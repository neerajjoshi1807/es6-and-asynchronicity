function* oddNumberFn() {
 let i = 0;
 while (true) {
 if (i % 2 !== 0) {
 yield i;
 }
 i += 1;
 }
}
var on = oddNumberFn();
for (let num of on) {
 if (num > 25) {
 break;
 }
 console.log(num);
}
module.exports=
{
    oddNumberFn:oddNumberFn
}
