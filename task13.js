let up=(a,err)=>
{
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            if(err==false)
            {
                var str=a.toUpperCase();
                resolve(str);
            }
            else{
                reject("Error occured!");
            }
        }

        ,500);
    }

    );

};
up().then(args=>
{
    console.log(args);
}

).catch(arg=>
{
    console.log(arg);
});
module.exports = { up: up };

