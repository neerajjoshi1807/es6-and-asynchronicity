let loadData=async(val)=>
{
    if(val>0)
    return({data:val});
    else
        throw new Error("Value must be greater than 0");

};
loadData().then(
    val=>
    {
        console.log(val);
    }
).catch(val2=>
{
    console.log(val2.message);
});
module.exports = {
 data: loadData
};
