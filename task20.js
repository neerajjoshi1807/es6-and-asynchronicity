async function concatenated(s1,s2)
{
    let ss1=await func1(s1);
    let ss2=await func2(s2);
    let con=ss1+ss2;
    return(con);
};
function func1(s1)
{
    return new Promise(resolve=>
    {
        setTimeout(()=>
        {
            resolve(s1);
        },5000);
    });
}
function func2(s2)
{
    return (s2);
};
module.exports = {
 concatenated: concatenated
};
