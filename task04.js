function add(x,y)
{
    return(x+y);
}
function sub(x,y)
{
    return(x-y);
}
function calc(x,y,callback)
{
    return callback(x,y);
}
module.exports = {
 add: add,
 sub: sub,
 calc: calc
};
