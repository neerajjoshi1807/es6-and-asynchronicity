function *generatorFn(num)
{
    var i=num;
    while(i>0)
    {
        i=i-1;
        yield i;
        
    }
}
var obj=generatorFn(10);
module.exports=
{
    generatorFn:generatorFn
}
