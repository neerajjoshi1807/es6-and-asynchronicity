let hello = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (true) {
        resolve("Hello World!");
      } else {
        reject("Error occured!");
      }
    }, 500);
  });
};

hello()
  .then(result => {
    console.log(result);
  })
  .catch(result => {
    console.log(result);
  });

