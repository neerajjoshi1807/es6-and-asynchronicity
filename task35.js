function *filterFn(array)
{
    let arr=array;
    let l=arr.length;
    var i=0;
    while(i<l)
    {
        if(i%2===0)
        {
            arr[i]=0;
            yield arr;
        }
        i=i+1;
    }
}
module.exports=
{
    filterFn:filterFn
}
