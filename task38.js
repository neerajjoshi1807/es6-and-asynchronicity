function *generator1(max)
{
    [pre,second]=[0,1];
    yield pre;
    
    while(second<max)
    {
        yield second;
        [pre,second]=[second,pre+second];
    }
}
function *generator2(max)
{
    let obj1=generator1(max);
    for(let i of obj1)
    {
        if(i%2===0)
        i=0;
        yield i;
    }
}
function *function3(max)
{
    let obj2=generator2(max);
    var arr=[];
    for(let j of obj2)
    {
        arr.push(j);
    }
    yield arr;

}
module.exports = {
 fibonacciFn: generator1,
 filterFn: generator2,
 checkFn: function3
};
