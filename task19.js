function f()
{
    return new Promise(resolve=>
    {
       setTimeout(()=>
       {
           resolve("done!")
       },1000) 
    })
};
async function f1()
{
    var i=await f();
    return(i);
};
module.exports = {
  func: f1
};


