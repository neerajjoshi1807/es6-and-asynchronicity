var fs=require("fs");
let readFile=(s)=>
{
    return new Promise((resolve,reject)=>
    {
        fs.readFile(s,(err,data)=>
        {
            if(!err)
            resolve(data.toString());
            else{
                reject("No file found");
            }
            
            

        });
    }

    );
    

};
readFile()
  .then(result => {
    console.log(result);
  })
  .catch(err => {
    console.log(err);
  });

module.exports = {
 readFile: readFile
};
