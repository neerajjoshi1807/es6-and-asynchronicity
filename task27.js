function timeout(num)
{
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            resolve();
        },num)
    }

    );
};
function resolveAfter1Second()
{
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            resolve("1 sec");

        },1000);

    });
};
function resolveAfter2Second()
{
    return new Promise((resolve,reject)=>
    {
        setTimeout(()=>
        {
            resolve("2 sec");

        },2000);

    });
};
async function sequentialStart()
{
    var start=await resolveAfter1Second();
    var end=await resolveAfter2Second();
    var obj={slow:end,fast:start};
    return (obj);
};
module.exports = sequentialStart;

