let add=(x,y)=>
{
    return new Promise((resolve,reject)=>
    {
        if(x>=0)
        {
            var sum=x+y;
            resolve(sum);
        }
        else
        {
            reject("x should be greater than 0");
        }
    }


    );

};
add().then(args=>
{
    console.log(args);
}).catch(arg=>
{
    console.log(arg);
});
module.exports = { add: add };
