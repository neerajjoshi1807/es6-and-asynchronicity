var fs = require("fs");
function rf() {
  return new Promise(function(resolve, reject) {
    fs.readFile("helper.txt", (err, data) => {
      //fs.readFIle is an inbuilt function in fs module for reading a file.
      if (err) {
        reject("No file found");
      } else {
        resolve(data.toString()); //toString decodes the buffer value into string format
      }
    });
  });
}

rf()
  .then(result => {
    console.log(result);
  })
  .catch(err => {
    console.log(err);
  });

