var fs=require("fs");
var fs=require("fs");
let readFile=(fname)=>
{
    return new Promise((resolve,reject)=>
    {
        fs.readFile(fname,(err,data)=>
        {
            if(!err)
            resolve(data.toString());
            else{
                reject("No file found");
            }
            
            

        });
    }

    );
    

};
let writeFile=(fname,s)=>
{
    return new Promise((resolve,reject)=>
    {
        fs.writeFile(fname,s,(err,data)=>
        {
            if(!err)
            resolve("Done!");
            else
            reject("Failed");
        }
        );
    }

    );

};
module.exports = {
 readFile: readFile,
 writeFile: writeFile
};

