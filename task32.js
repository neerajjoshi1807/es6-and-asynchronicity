function *evenNumberFn()
{
    var i=0;
    while(true)
    {
        if(i%2==0)
        {
            yield i;
        }
        i=i+1;
    }
};
var obj=evenNumberFn();
for(var k of obj)
{
    if(k>25)
    break;
    console.log(k);
}
module.exports=
{
    evenNumberFn:evenNumberFn
}
