function b()
{
    return("Hello!!!");
}
function a(callback)
{
    return callback(b);
}
module.exports = {
 first: a,
 second: b
};
